{
  "label": "Revenue Drivers Analysis",
  "category_label": "Models",
  "branding": {
    "image_uri": "https://www.tatvic.com/wp-content/uploads/2017/02/cropped-sq-logo.png",
    "tagline": "This block is about quantifying impact of key drivers of revenue change (Users, Average Price, Conversion Rate, Cart Size) for an ecommerce player.  This can be used to frame revenue growth strategy and make timely interventions."
  },
  "constants": {
    "CONNECTION": {
      "label": "Connection Name",
      "value_constraint": "connection"
    },
    "SCHEMA": {
      "label": "GA4 Schema"
    },
    "TABLE_VARIABLE": {
        "label": "GA4 Table Variable"
    }
  },
  "models": [
    {
      "name": "ecom_ga4",
      "connection_constant": "CONNECTION"
    }
  ]
}
