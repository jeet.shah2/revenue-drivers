# include: "/views/ecom_transaction.view.lkml"

view: sessions {
  derived_table: {
    datagroup_trigger: ga4_ecom_datagroup
    partition_keys: ["session_date"]
    cluster_keys: ["session_date"]
    increment_key: "session_date"
    increment_offset: 3
    sql:
      SELECT
      timestamp(PARSE_DATE('%Y%m%d', REGEXP_EXTRACT(_TABLE_SUFFIX,r'[0-9]+'))) session_date
      ,ecommerce.purchase_revenue
      ,ecommerce.transaction_id
      ,user_pseudo_id
      ,(SELECT   ep.value.string_value from UNNEST(event_params) ep where ep.key='is_active_user') is_active_user
      ,device.category
      ,geo.country
      ,geo.region
      ,geo.city
      ,traffic_source.name
      ,traffic_source.medium
      ,traffic_source.source
      ,items.item_id
      ,items.item_name
      ,items.item_brand
      ,items.item_variant
      ,items.item_category
      ,items.price
      ,items.quantity
      ,items.item_revenue
      ,event_name
      from `@{SCHEMA}.@{TABLE_VARIABLE}` events, unnest(items) items
      where  {% incrementcondition %} timestamp(PARSE_DATE('%Y%m%d', REGEXP_EXTRACT(_TABLE_SUFFIX,r'[0-9]+'))) {%  endincrementcondition %}
      ;;
  }

  dimension: category {
    type: string
    sql: ${TABLE}.category ;;
  }
  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }
  dimension: is_active_user {
    type: string
    sql: ${TABLE}.is_active_user ;;
  }
  dimension: item_brand {
    type: string
    sql: ${TABLE}.item_brand ;;
  }
  dimension: item_category {
    type: string
    sql: ${TABLE}.item_category ;;
  }

  dimension: item_id {
    type: string
    sql: ${TABLE}.item_id ;;
  }
  dimension: item_name {
    type: string
    sql: ${TABLE}.item_name ;;
  }
  dimension: item_revenue {
    type: number
    sql: ${TABLE}.item_revenue ;;
  }

  dimension: item_variant {
    type: string
    sql: ${TABLE}.item_variant ;;
  }
  dimension: medium {
    type: string
    sql: ${TABLE}.medium ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }
  dimension: platform {
    type: string
    sql: ${TABLE}.platform ;;
  }
  dimension: price {
    type: number
    sql: ${TABLE}.price ;;
  }
  dimension: purchase_revenue {
    type: number
    sql: ${TABLE}.purchase_revenue ;;
  }
  dimension: quantity {
    type: number
    sql: ${TABLE}.quantity ;;
  }
  dimension: region {
    type: string
    sql: ${TABLE}.region ;;
  }
  dimension_group: session {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.session_date ;;
  }
  dimension: source {
    type: string
    sql: ${TABLE}.source ;;
  }
  dimension: stream_id {
    type: number
    sql: ${TABLE}.stream_id ;;
  }

  dimension: transaction_id {
    type: string
    sql: ${TABLE}.transaction_id ;;
  }

  dimension: user_pseudo_id {
    type: string
    sql: ${TABLE}.user_pseudo_id ;;
  }

  dimension: event_name  {
    type: string
    sql: ${TABLE}.event_name ;;
  }




}
