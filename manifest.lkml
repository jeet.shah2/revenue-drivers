project_name: "block-revenue-drivers-ga4"

################# Constants ################

## Used in ecom_ga4.model connection param
constant: CONNECTION {
  # value: "tatvic-ga4-262420034"
  # value: "e_commerce_poc"

  value: "starbucks_ga4"

  # export: override_required
}

## Used in sessions.view sql_table_name
constant: SCHEMA {
  # value: "level-elevator-748.analytics_262420034"
  value: "tata-starbucks-prod-40042.analytics_332157659"
  export: override_optional
}

constant: TABLE_VARIABLE {
  value: "events_*"
  export: override_optional
}
