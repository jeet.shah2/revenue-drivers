- dashboard: 2__ecommerce__rca__conversion_rate_drilldown
  title: 2. Ecommerce - RCA - Conversion Rate Drill-down
  layout: newspaper
  preferred_viewer: dashboards-next
  crossfilter_enabled: true
  description: ''
  preferred_slug: eweWRk6zKMblr8YcCTVtzB
  elements:
  - name: ''
    type: text
    title_text: ''
    subtitle_text: ''
    body_text: '[{"type":"ul","children":[{"type":"li","children":[{"type":"lic","children":[{"text":"We
      saw in earlier dashboard that Revenue change is driven by 4 factors namely :
      Users, Conversion Rate, Cart Size, Average Price.","fontSize":"12pt","backgroundColor":"transparent","color":"rgb(67,
      67, 67)"}],"id":1692867676628}],"id":1692689814319},{"type":"li","children":[{"type":"lic","children":[{"text":"In
      this dashboard, we are focusing to deep dive in the contribution made by Conversion
      Rate.","fontSize":"12pt","backgroundColor":"transparent","color":"rgb(67, 67,
      67)"}],"id":1692867723292}],"id":1692867723292}],"id":1693570909041}]'
    rich_content_json: '{"format":"slate"}'
    row: 2
    col: 0
    width: 24
    height: 3
  - title: Conversion Rate
    name: Conversion Rate
    model: ecom_ga4
    explore: sessions
    type: single_value
    fields: [total_users, purchase_users]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: measure
      expression: ''
      label: Total Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: total_users
      type: count_distinct
      _type_hint: number
    - category: table_calculation
      expression: "${purchase_users}/${total_users}"
      label: Conversion Rate
      value_format:
      value_format_name: percent_2
      _kind_hint: measure
      table_calculation: conversion_rate
      _type_hint: number
    - category: measure
      expression: ''
      label: Purchase Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: purchase_users
      type: count_distinct
      _type_hint: number
      filters:
        sessions.event_name: purchase
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: ''
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: [count_of_user_pseudo_id, total_users, purchase_users]
    hidden_pivots: {}
    listen:
      Month: sessions.session_month
      City: sessions.city
      Region: sessions.region
      Item Category: sessions.item_category
    row: 6
    col: 18
    width: 6
    height: 2
  - title: Revenue
    name: Revenue
    model: ecom_ga4
    explore: sessions
    type: single_value
    fields: [sum_of_purchase_revenue]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - measure: sum_of_purchase_revenue
      based_on: sessions.purchase_revenue
      expression: ''
      label: Sum of Purchase Revenue
      type: sum
      _kind_hint: measure
      _type_hint: number
    - category: table_calculation
      expression: sum(${sum_of_purchase_revenue})
      label: Revenue
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: revenue
      _type_hint: number
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: '"$" #,##0,,"M"'
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: [sum_of_purchase_revenue]
    listen:
      Month: sessions.session_month
      City: sessions.city
      Region: sessions.region
      Item Category: sessions.item_category
    row: 6
    col: 0
    width: 6
    height: 2
  - title: Purchasers
    name: Purchasers
    model: ecom_ga4
    explore: sessions
    type: single_value
    fields: [count_of_item_id, unique_purchasers]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - measure: count_of_quantity
      based_on: sessions.quantity
      expression: ''
      label: Count of Quantity
      type: count_distinct
      _kind_hint: measure
      _type_hint: number
    - category: measure
      expression: ''
      label: Count of Item ID
      based_on: sessions.item_id
      _kind_hint: measure
      measure: count_of_item_id
      type: count_distinct
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Unique Purchasers
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: unique_purchasers
      type: count_distinct
      _type_hint: number
      filters:
        sessions.event_name: purchase
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: "#,##0"
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: [count_of_item_id]
    hidden_pivots: {}
    listen:
      Month: sessions.session_month
      City: sessions.city
      Region: sessions.region
      Item Category: sessions.item_category
    row: 6
    col: 12
    width: 6
    height: 2
  - title: Total Users
    name: Total Users
    model: ecom_ga4
    explore: sessions
    type: single_value
    fields: [sum_of_purchase_revenue, sum_of_quantity, count_of_user_pseudo_id]
    filters:
      sessions.event_name: ''
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}/${sum_of_quantity}"
      label: Average Price
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: average_price
      _type_hint: number
    - measure: sum_of_purchase_revenue
      based_on: sessions.purchase_revenue
      expression: ''
      label: Sum of Purchase Revenue
      type: sum
      _kind_hint: measure
      _type_hint: number
    - category: measure
      expression: ''
      label: Sum of Quantity
      based_on: sessions.quantity
      _kind_hint: measure
      measure: sum_of_quantity
      type: sum
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Count of User Pseudo ID
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: count_of_user_pseudo_id
      type: count_distinct
      _type_hint: number
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: "#,##0"
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: [sum_of_purchase_revenue, sum_of_quantity, average_price]
    hidden_pivots: {}
    listen:
      Month: sessions.session_month
      City: sessions.city
      Region: sessions.region
      Item Category: sessions.item_category
    row: 6
    col: 6
    width: 6
    height: 2
  - title: 'AP : Revenue Decomposition by Regions'
    name: 'AP : Revenue Decomposition by Regions'
    model: ecom_ga4
    explore: sessions
    type: looker_column
    fields: [sessions.session_month, count_of_transaction_id, sum_of_purchase_revenue,
      sum_of_quantity, purchase_revenue_current, total_users, purchase_users, sessions.region]
    pivots: [sessions.region]
    fill_fields: [sessions.session_month]
    filters: {}
    sorts: [sessions.region, sessions.session_month]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: measure
      expression: ''
      label: Count of Transaction ID
      based_on: sessions.transaction_id
      _kind_hint: measure
      measure: count_of_transaction_id
      type: count_distinct
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Sum of Purchase Revenue
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: sum_of_purchase_revenue
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Sum of Quantity
      based_on: sessions.quantity
      _kind_hint: measure
      measure: sum_of_quantity
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Purchase Revenue - Current
      value_format:
      value_format_name: usd_0
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: purchase_revenue_current
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Total Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: total_users
      type: count_distinct
      _type_hint: number
    - category: table_calculation
      expression: offset(${purchase_revenue_current},-1)
      label: Purchase Revenue - Previous
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: purchase_revenue_previous
      _type_hint: number
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}-offset(${sum_of_purchase_revenue},-1)"
      label: Revenue Change
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: revenue_change
      _type_hint: number
    - category: table_calculation
      expression: "${count_of_transaction_id}/${total_users}"
      label: Conversion Rate
      value_format:
      value_format_name: percent_0
      _kind_hint: measure
      table_calculation: conversion_rate
      _type_hint: number
    - category: table_calculation
      expression: "${sum_of_quantity}/${count_of_transaction_id}"
      label: Cart Size
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: cart_size
      _type_hint: number
    - category: measure
      expression: ''
      label: Purchase Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: purchase_users
      type: count_distinct
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}/${sum_of_quantity}"
      label: Average Price
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: average_price
      _type_hint: number
    - category: table_calculation
      expression: "${revenue_change}/(log(${purchase_revenue_current})-log(${purchase_revenue_previous}))"
      label: Multiplying Factor
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: multiplying_factor
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${average_price}/offset(${average_price},-1)))"
      label: Average Price Contribution
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: average_price_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${conversion_rate}/offset(${conversion_rate},-1)))"
      label: Conversion Rate Contribution
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: conversion_rate_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${cart_size}/offset(${cart_size},-1)))"
      label: 'Cart Size Contribution '
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: cart_size_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${total_users}/offset(${total_users},-1)))"
      label: 'Users Contribution  '
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: users_contribution
      _type_hint: number
    - category: dimension
      expression: concat(${sessions.source}," / ",${sessions.medium})
      label: Source / Medium
      value_format:
      value_format_name:
      dimension: source_medium
      _kind_hint: dimension
      _type_hint: string
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: percent
    limit_displayed_rows: false
    legend_position: center
    point_style: circle_outline
    show_value_labels: true
    label_density: 25
    x_axis_scale: time
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    color_application:
      collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
      palette_id: 5d189dfc-4f46-46f3-822b-bfb0b61777b1
      options:
        steps: 5
        reverse: false
    y_axes: [{label: '', orientation: left, series: [{axisId: average_price_contribution,
            id: average_price_contribution, name: Average Price Contribution}, {axisId: conversion_rate_contribution,
            id: conversion_rate_contribution, name: Conversion Rate Contribution},
          {axisId: cart_size_contribution, id: cart_size_contribution, name: 'Cart
              Size Contribution '}, {axisId: users_contribution, id: users_contribution,
            name: 'Users Contribution  '}], showLabels: true, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear}]
    x_axis_zoom: true
    y_axis_zoom: true
    limit_displayed_rows_values:
      show_hide: hide
      first_last: first
      num_rows: 0
    hide_legend: false
    font_size: '10'
    series_colors: {}
    series_labels:
      sum_of_purchase_revenue: Revenue
    series_point_styles:
      sum_of_purchase_revenue: auto
    x_axis_datetime_label: ''
    reference_lines: []
    trend_lines: []
    show_null_points: true
    interpolation: linear
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: ''
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: [multiplying_factor, average_price, cart_size, conversion_rate,
      revenue_change, purchase_revenue_previous, purchase_users, total_users, purchase_revenue_current,
      sum_of_quantity, sum_of_purchase_revenue, count_of_transaction_id, cart_size_contribution,
      users_contribution, average_price_contribution]
    hidden_pivots: {}
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    title_hidden: true
    listen:
      Month: sessions.session_month
      City: sessions.city
      Region: sessions.region
      Item Category: sessions.item_category
    row: 23
    col: 12
    width: 12
    height: 7
  - title: 'CR : Revenue Decomposition by Device Category'
    name: 'CR : Revenue Decomposition by Device Category'
    model: ecom_ga4
    explore: sessions
    type: looker_column
    fields: [sessions.session_month, count_of_transaction_id, sum_of_purchase_revenue,
      sum_of_quantity, purchase_revenue_current, total_users, purchase_users, sessions.category]
    pivots: [sessions.category]
    fill_fields: [sessions.session_month]
    sorts: [sessions.category, sessions.session_month]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: measure
      expression: ''
      label: Count of Transaction ID
      based_on: sessions.transaction_id
      _kind_hint: measure
      measure: count_of_transaction_id
      type: count_distinct
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Sum of Purchase Revenue
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: sum_of_purchase_revenue
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Sum of Quantity
      based_on: sessions.quantity
      _kind_hint: measure
      measure: sum_of_quantity
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Purchase Revenue - Current
      value_format:
      value_format_name: usd_0
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: purchase_revenue_current
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Total Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: total_users
      type: count_distinct
      _type_hint: number
    - category: table_calculation
      expression: offset(${purchase_revenue_current},-1)
      label: Purchase Revenue - Previous
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: purchase_revenue_previous
      _type_hint: number
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}-offset(${sum_of_purchase_revenue},-1)"
      label: Revenue Change
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: revenue_change
      _type_hint: number
    - category: table_calculation
      expression: "${count_of_transaction_id}/${total_users}"
      label: Conversion Rate
      value_format:
      value_format_name: percent_0
      _kind_hint: measure
      table_calculation: conversion_rate
      _type_hint: number
    - category: table_calculation
      expression: "${sum_of_quantity}/${count_of_transaction_id}"
      label: Cart Size
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: cart_size
      _type_hint: number
    - category: measure
      expression: ''
      label: Purchase Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: purchase_users
      type: count_distinct
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}/${sum_of_quantity}"
      label: Average Price
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: average_price
      _type_hint: number
    - category: table_calculation
      expression: "${revenue_change}/(log(${purchase_revenue_current})-log(${purchase_revenue_previous}))"
      label: Multiplying Factor
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: multiplying_factor
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${average_price}/offset(${average_price},-1)))"
      label: Average Price Contribution
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: average_price_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${conversion_rate}/offset(${conversion_rate},-1)))"
      label: Conversion Rate Contribution
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: conversion_rate_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${cart_size}/offset(${cart_size},-1)))"
      label: 'Cart Size Contribution '
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: cart_size_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${total_users}/offset(${total_users},-1)))"
      label: 'Users Contribution  '
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: users_contribution
      _type_hint: number
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: percent
    limit_displayed_rows: false
    legend_position: center
    point_style: circle_outline
    show_value_labels: true
    label_density: 25
    x_axis_scale: time
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    color_application:
      collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
      palette_id: 5d189dfc-4f46-46f3-822b-bfb0b61777b1
      options:
        steps: 5
        reverse: false
    y_axes: [{label: '', orientation: left, series: [{axisId: average_price_contribution,
            id: average_price_contribution, name: Average Price Contribution}, {axisId: conversion_rate_contribution,
            id: conversion_rate_contribution, name: Conversion Rate Contribution},
          {axisId: cart_size_contribution, id: cart_size_contribution, name: 'Cart
              Size Contribution '}, {axisId: users_contribution, id: users_contribution,
            name: 'Users Contribution  '}], showLabels: true, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear}]
    x_axis_zoom: true
    y_axis_zoom: true
    limit_displayed_rows_values:
      show_hide: hide
      first_last: first
      num_rows: 0
    hide_legend: false
    font_size: '10'
    series_colors: {}
    series_labels:
      sum_of_purchase_revenue: Revenue
    series_point_styles:
      sum_of_purchase_revenue: auto
    x_axis_datetime_label: ''
    reference_lines: []
    trend_lines: []
    show_null_points: true
    interpolation: linear
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: ''
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: [multiplying_factor, average_price, cart_size, conversion_rate,
      revenue_change, purchase_revenue_previous, purchase_users, total_users, purchase_revenue_current,
      sum_of_quantity, sum_of_purchase_revenue, count_of_transaction_id, cart_size_contribution,
      users_contribution, average_price_contribution]
    hidden_pivots: {}
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    title_hidden: true
    listen:
      Month: sessions.session_month
      City: sessions.city
      Region: sessions.region
      Item Category: sessions.item_category
    row: 16
    col: 12
    width: 12
    height: 6
  - title: 'AP : Revenue Decomposition by Channels'
    name: 'AP : Revenue Decomposition by Channels'
    model: ecom_ga4
    explore: sessions
    type: looker_column
    fields: [sessions.session_month, count_of_transaction_id, sum_of_purchase_revenue,
      sum_of_quantity, purchase_revenue_current, total_users, purchase_users, channel]
    pivots: [channel]
    fill_fields: [sessions.session_month]
    sorts: [channel, sessions.session_month]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: measure
      expression: ''
      label: Count of Transaction ID
      based_on: sessions.transaction_id
      _kind_hint: measure
      measure: count_of_transaction_id
      type: count_distinct
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Sum of Purchase Revenue
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: sum_of_purchase_revenue
      type: sum
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Sum of Quantity
      based_on: sessions.quantity
      _kind_hint: measure
      measure: sum_of_quantity
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Purchase Revenue - Current
      value_format:
      value_format_name: usd_0
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: purchase_revenue_current
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Total Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: total_users
      type: count_distinct
      _type_hint: number
    - category: table_calculation
      expression: offset(${purchase_revenue_current},-1)
      label: Purchase Revenue - Previous
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: purchase_revenue_previous
      _type_hint: number
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}-offset(${sum_of_purchase_revenue},-1)"
      label: Revenue Change
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: revenue_change
      _type_hint: number
    - category: table_calculation
      expression: "${count_of_transaction_id}/${total_users}"
      label: Conversion Rate
      value_format:
      value_format_name: percent_0
      _kind_hint: measure
      table_calculation: conversion_rate
      _type_hint: number
    - category: table_calculation
      expression: "${sum_of_quantity}/${count_of_transaction_id}"
      label: Cart Size
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: cart_size
      _type_hint: number
    - category: measure
      expression: ''
      label: Purchase Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: purchase_users
      type: count_distinct
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}/${sum_of_quantity}"
      label: Average Price
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: average_price
      _type_hint: number
    - category: table_calculation
      expression: "${revenue_change}/(log(${purchase_revenue_current})-log(${purchase_revenue_previous}))"
      label: Multiplying Factor
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: multiplying_factor
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${average_price}/offset(${average_price},-1)))"
      label: Average Price Contribution
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: average_price_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${conversion_rate}/offset(${conversion_rate},-1)))"
      label: Conversion Rate Contribution
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: conversion_rate_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${cart_size}/offset(${cart_size},-1)))"
      label: 'Cart Size Contribution '
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: cart_size_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${total_users}/offset(${total_users},-1)))"
      label: 'Users Contribution  '
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: users_contribution
      _type_hint: number
    - category: dimension
      expression: concat(${sessions.source}," / ",${sessions.medium})
      label: Source / Medium
      value_format:
      value_format_name:
      dimension: source_medium
      _kind_hint: dimension
      _type_hint: string
    - category: dimension
      expression: |-
        case(
          when(${sessions.medium}="(none)","Direct"),
          when(${sessions.medium}="cpc","Paid"),
          when(${sessions.medium}="cpm","Paid"),
          when(${sessions.medium}="affiliate","Affiliate"),
          when(${sessions.medium}="organic","Organic"),
          when(${sessions.medium}="referral","Referral"),
          "Other"
          )
      label: Channel
      value_format:
      value_format_name:
      dimension: channel
      _kind_hint: measure
      _type_hint: string
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: percent
    limit_displayed_rows: false
    legend_position: center
    point_style: circle_outline
    show_value_labels: true
    label_density: 25
    x_axis_scale: time
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    color_application:
      collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
      palette_id: 5d189dfc-4f46-46f3-822b-bfb0b61777b1
      options:
        steps: 5
        reverse: false
    y_axes: [{label: '', orientation: left, series: [{axisId: average_price_contribution,
            id: average_price_contribution, name: Average Price Contribution}, {axisId: conversion_rate_contribution,
            id: conversion_rate_contribution, name: Conversion Rate Contribution},
          {axisId: cart_size_contribution, id: cart_size_contribution, name: 'Cart
              Size Contribution '}, {axisId: users_contribution, id: users_contribution,
            name: 'Users Contribution  '}], showLabels: true, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear}]
    x_axis_zoom: true
    y_axis_zoom: true
    limit_displayed_rows_values:
      show_hide: hide
      first_last: first
      num_rows: 0
    hide_legend: false
    font_size: '10'
    series_colors: {}
    series_labels:
      sum_of_purchase_revenue: Revenue
    series_point_styles:
      sum_of_purchase_revenue: auto
    x_axis_datetime_label: ''
    reference_lines: []
    trend_lines: []
    show_null_points: true
    interpolation: linear
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: ''
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: [multiplying_factor, average_price, cart_size, conversion_rate,
      revenue_change, purchase_revenue_previous, purchase_users, total_users, purchase_revenue_current,
      sum_of_quantity, sum_of_purchase_revenue, count_of_transaction_id, cart_size_contribution,
      users_contribution, average_price_contribution]
    hidden_pivots: {}
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    title_hidden: true
    listen:
      Month: sessions.session_month
      City: sessions.city
      Region: sessions.region
      Item Category: sessions.item_category
    row: 16
    col: 0
    width: 12
    height: 6
  - title: 'AP : Revenue Decomposition by Product Categories'
    name: 'AP : Revenue Decomposition by Product Categories'
    model: ecom_ga4
    explore: sessions
    type: looker_column
    fields: [sessions.session_month, count_of_transaction_id, sum_of_purchase_revenue,
      sum_of_quantity, purchase_revenue_current, total_users, purchase_users, sessions.item_category]
    pivots: [sessions.item_category]
    fill_fields: [sessions.session_month]
    filters:
      sessions.medium: "-(none),-(not set)"
    sorts: [sessions.item_category, sessions.session_month]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: measure
      expression: ''
      label: Count of Transaction ID
      based_on: sessions.transaction_id
      _kind_hint: measure
      measure: count_of_transaction_id
      type: count_distinct
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Sum of Purchase Revenue
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: sum_of_purchase_revenue
      type: sum
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Sum of Quantity
      based_on: sessions.quantity
      _kind_hint: measure
      measure: sum_of_quantity
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Purchase Revenue - Current
      value_format:
      value_format_name: usd_0
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: purchase_revenue_current
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Total Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: total_users
      type: count_distinct
      _type_hint: number
    - category: table_calculation
      expression: offset(${purchase_revenue_current},-1)
      label: Purchase Revenue - Previous
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: purchase_revenue_previous
      _type_hint: number
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}-offset(${sum_of_purchase_revenue},-1)"
      label: Revenue Change
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: revenue_change
      _type_hint: number
    - category: table_calculation
      expression: "${count_of_transaction_id}/${total_users}"
      label: Conversion Rate
      value_format:
      value_format_name: percent_0
      _kind_hint: measure
      table_calculation: conversion_rate
      _type_hint: number
    - category: table_calculation
      expression: "${sum_of_quantity}/${count_of_transaction_id}"
      label: Cart Size
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: cart_size
      _type_hint: number
    - category: measure
      expression: ''
      label: Purchase Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: purchase_users
      type: count_distinct
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}/${sum_of_quantity}"
      label: Average Price
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: average_price
      _type_hint: number
    - category: table_calculation
      expression: "${revenue_change}/(log(${purchase_revenue_current})-log(${purchase_revenue_previous}))"
      label: Multiplying Factor
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: multiplying_factor
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${average_price}/offset(${average_price},-1)))"
      label: Average Price Contribution
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: average_price_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${conversion_rate}/offset(${conversion_rate},-1)))"
      label: Conversion Rate Contribution
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: conversion_rate_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${cart_size}/offset(${cart_size},-1)))"
      label: 'Cart Size Contribution '
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: cart_size_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${total_users}/offset(${total_users},-1)))"
      label: 'Users Contribution  '
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: users_contribution
      _type_hint: number
    - category: dimension
      expression: concat(${sessions.source}," / ",${sessions.medium})
      label: Source / Medium
      value_format:
      value_format_name:
      dimension: source_medium
      _kind_hint: dimension
      _type_hint: string
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: percent
    limit_displayed_rows: false
    legend_position: center
    point_style: circle_outline
    show_value_labels: true
    label_density: 25
    x_axis_scale: time
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    color_application:
      collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
      palette_id: 5d189dfc-4f46-46f3-822b-bfb0b61777b1
      options:
        steps: 5
        reverse: false
    y_axes: [{label: '', orientation: left, series: [{axisId: average_price_contribution,
            id: average_price_contribution, name: Average Price Contribution}, {axisId: conversion_rate_contribution,
            id: conversion_rate_contribution, name: Conversion Rate Contribution},
          {axisId: cart_size_contribution, id: cart_size_contribution, name: 'Cart
              Size Contribution '}, {axisId: users_contribution, id: users_contribution,
            name: 'Users Contribution  '}], showLabels: true, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear}]
    x_axis_zoom: true
    y_axis_zoom: true
    limit_displayed_rows_values:
      show_hide: hide
      first_last: first
      num_rows: 0
    hide_legend: false
    font_size: '10'
    series_colors: {}
    series_labels:
      sum_of_purchase_revenue: Revenue
    series_point_styles:
      sum_of_purchase_revenue: auto
    x_axis_datetime_label: ''
    reference_lines: []
    trend_lines: []
    show_null_points: true
    interpolation: linear
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: ''
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: [multiplying_factor, average_price, cart_size, conversion_rate,
      revenue_change, purchase_revenue_previous, purchase_users, total_users, purchase_revenue_current,
      sum_of_quantity, sum_of_purchase_revenue, count_of_transaction_id, cart_size_contribution,
      users_contribution, average_price_contribution]
    hidden_pivots: {}
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    title_hidden: true
    listen:
      Month: sessions.session_month
      City: sessions.city
      Region: sessions.region
      Item Category: sessions.item_category
    row: 23
    col: 0
    width: 12
    height: 7
  - name: " (2)"
    type: text
    title_text: ''
    subtitle_text: ''
    body_text: '[{"type":"p","children":[{"text":""}],"id":1692971024532},{"type":"h1","children":[{"text":"INTRODUCTION","color":"hsl(244,
      100%, 50%)","bold":true,"underline":true}],"id":1692970959905}]'
    rich_content_json: '{"format":"slate"}'
    row: 0
    col: 10
    width: 14
    height: 2
  - name: <img src="https://cdn-icons-pngflaticoncom/512/4784/4784180png" width="60"
      height="60">
    type: text
    title_text: <img src="https://cdn-icons-png.flaticon.com/512/4784/4784180.png"
      width="60" height="60">
    subtitle_text: ''
    body_text: ''
    row: 0
    col: 7
    width: 3
    height: 2
  - name: '<img src="https://cdn-icons-pngflaticoncom/512/10219/10219302png" width="30"
      height="30"> DEVICE CATEGORY CONTRIBUTION</span>   '
    type: text
    title_text: '<img src="https://cdn-icons-png.flaticon.com/512/10219/10219302.png"
      width="30" height="30"> DEVICE CATEGORY CONTRIBUTION</span>   '
    subtitle_text: ''
    body_text: ''
    row: 15
    col: 12
    width: 12
    height: 1
  - name: '<img src="https://staticvecteezycom/system/resources/previews/019/507/840/non_2x/revenue-icon-design-free-vectorjpg"
      width="30" height="30"> MONTHLY REVENUE CHANGE LED BY CONV RATE</span> '
    type: text
    title_text: '<img src="https://static.vecteezy.com/system/resources/previews/019/507/840/non_2x/revenue-icon-design-free-vector.jpg"
      width="30" height="30"> MONTHLY REVENUE CHANGE LED BY CONV RATE</span> '
    subtitle_text: ''
    body_text: ''
    row: 8
    col: 0
    width: 24
    height: 1
  - name: '<img src="https://encrypted-tbn0gstaticcom/images?q=tbn:ANd9GcRsDj90v_TOy6CnPBd5UgzsiYD2Qtq43_2PMA&amp;usqp=CAU"
      width="30" height="30"> CHANNEL CONTRIBUTION</span>  '
    type: text
    title_text: '<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRsDj90v_TOy6CnPBd5UgzsiYD2Qtq43_2PMA&amp;usqp=CAU"
      width="30" height="30"> CHANNEL CONTRIBUTION</span>  '
    subtitle_text: ''
    body_text: ''
    row: 15
    col: 0
    width: 12
    height: 1
  - name: '<img src="https://cdn-icons-pngflaticoncom/128/2947/2947660png" width="30"
      height="30"> REGION DECOMPOSITION</span> '
    type: text
    title_text: '<img src="https://cdn-icons-png.flaticon.com/128/2947/2947660.png"
      width="30" height="30"> REGION DECOMPOSITION</span> '
    subtitle_text: ''
    body_text: ''
    row: 22
    col: 12
    width: 12
    height: 1
  - name: ' <img src="https://w7pngwingcom/pngs/201/722/png-transparent-computer-icons-speedometer-dashboard-speedometer-angle-motorcycle-odometer-thumbnailpng"
      width="30" height="30">  Top Line Metrics</span> '
    type: text
    title_text: ' <img src="https://w7.pngwing.com/pngs/201/722/png-transparent-computer-icons-speedometer-dashboard-speedometer-angle-motorcycle-odometer-thumbnail.png"
      width="30" height="30">  Top Line Metrics</span> '
    subtitle_text: ''
    body_text: ''
    row: 5
    col: 0
    width: 24
    height: 1
  - name: '<img src="https://cdn-icons-pngflaticoncom/512/2311/2311879png" width="30"
      height="30"> PRODUCT CATEGORY DECOMPOSITION</span>  '
    type: text
    title_text: '<img src="https://cdn-icons-png.flaticon.com/512/2311/2311879.png"
      width="30" height="30"> PRODUCT CATEGORY DECOMPOSITION</span>  '
    subtitle_text: ''
    body_text: ''
    row: 22
    col: 1
    width: 11
    height: 1
  - title: New Tile
    name: New Tile
    model: ecom_ga4
    explore: sessions
    type: looker_line
    fields: [sessions.session_month, count_of_transaction_id, sum_of_purchase_revenue,
      product_quantity, purchase_revenue_current, total_users, purchase_users, purchase_quantity]
    fill_fields: [sessions.session_month]
    filters:
      sessions.source: ''
      sessions.item_name: ''
      sessions.medium: ''
      sessions.country: ''
    sorts: [sessions.session_month]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: measure
      expression: ''
      label: Count of Transaction ID
      based_on: sessions.transaction_id
      _kind_hint: measure
      measure: count_of_transaction_id
      type: count_distinct
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Sum of Purchase Revenue
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: sum_of_purchase_revenue
      type: sum
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Product Quantity
      based_on: sessions.quantity
      _kind_hint: measure
      measure: product_quantity
      type: sum
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Purchase Revenue - Current
      value_format:
      value_format_name: decimal_0
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: purchase_revenue_current
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Total Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: total_users
      type: count_distinct
      _type_hint: number
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}-offset(${sum_of_purchase_revenue},-1)"
      label: Revenue Change
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: revenue_change
      _type_hint: number
    - category: table_calculation
      expression: "${purchase_users}/${total_users}"
      label: Conversion Rate
      value_format:
      value_format_name: percent_0
      _kind_hint: measure
      table_calculation: conversion_rate
      _type_hint: number
    - category: table_calculation
      expression: "${purchase_quantity}/${count_of_transaction_id}"
      label: Cart Size
      value_format:
      value_format_name: decimal_2
      _kind_hint: measure
      table_calculation: cart_size
      _type_hint: number
    - category: measure
      expression: ''
      label: Purchase Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: purchase_users
      type: count_distinct
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}/${purchase_quantity}"
      label: Average Price
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: average_price
      _type_hint: number
    - category: table_calculation
      expression: "${revenue_change}/(log(${purchase_revenue_current})-log(offset(${purchase_revenue_current},-1)))"
      label: Multiplying Factor
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: multiplying_factor
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${average_price}/offset(${average_price},-1)))"
      label: Average Price Contribution
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: average_price_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${conversion_rate}/offset(${conversion_rate},-1)))"
      label: Conversion Rate Contribution
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: conversion_rate_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${cart_size}/offset(${cart_size},-1)))"
      label: 'Cart Size Contribution '
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: cart_size_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${total_users}/offset(${total_users},-1)))"
      label: 'Users Contribution  '
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: users_contribution
      _type_hint: number
    - category: measure
      expression:
      label: Purchase Quantity
      value_format:
      value_format_name:
      based_on: sessions.quantity
      _kind_hint: measure
      measure: purchase_quantity
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: circle_outline
    show_value_labels: true
    label_density: 25
    x_axis_scale: time
    y_axis_combined: true
    show_null_points: true
    interpolation: linear
    color_application:
      collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
      palette_id: 5d189dfc-4f46-46f3-822b-bfb0b61777b1
      options:
        steps: 5
    y_axes: [{label: '', orientation: left, series: [{axisId: average_price_contribution,
            id: average_price_contribution, name: Average Price Contribution}, {axisId: conversion_rate_contribution,
            id: conversion_rate_contribution, name: Conversion Rate Contribution},
          {axisId: cart_size_contribution, id: cart_size_contribution, name: 'Cart
              Size Contribution '}, {axisId: users_contribution, id: users_contribution,
            name: 'Users Contribution  '}], showLabels: true, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear}]
    x_axis_zoom: true
    y_axis_zoom: true
    font_size: '14'
    label_value_format: '"$" #,##0'
    series_colors:
      average_price_contribution: "#7CB342"
      cart_size_contribution: "#80868B"
      users_contribution: "#EA4335"
    series_labels:
      sum_of_purchase_revenue: Revenue
    series_point_styles:
      sum_of_purchase_revenue: auto
      conversion_rate: diamond
      cart_size: diamond
    x_axis_datetime_label: ''
    reference_lines: []
    trend_lines: []
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: ''
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: [multiplying_factor, revenue_change, purchase_revenue_previous,
      purchase_users, total_users, purchase_revenue_current, sum_of_quantity, sum_of_purchase_revenue,
      count_of_transaction_id, purchase_quantity, product_quantity, cart_size, cart_size_contribution,
      users_contribution, conversion_rate, average_price, average_price_contribution]
    hidden_pivots: {}
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    title_hidden: true
    listen:
      City: sessions.city
      Month: sessions.session_month
      Item Category: sessions.item_category
      Region: sessions.region
    row: 9
    col: 0
    width: 24
    height: 6
  filters:
  - name: Month
    title: Month
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: advanced
      display: popover
      options: []
    model: ecom_ga4
    explore: sessions
    listens_to_filters: []
    field: sessions.session_month
  - name: City
    title: City
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: tag_list
      display: popover
    model: ecom_ga4
    explore: sessions
    listens_to_filters: []
    field: sessions.city
  - name: Region
    title: Region
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: tag_list
      display: popover
    model: ecom_ga4
    explore: sessions
    listens_to_filters: []
    field: sessions.region
  - name: Item Category
    title: Item Category
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: tag_list
      display: popover
    model: ecom_ga4
    explore: sessions
    listens_to_filters: []
    field: sessions.item_category
