- dashboard: 1__ecommerce__revenue_contribution_analysis
  title: 1. Ecommerce - Revenue Contribution Analysis
  layout: newspaper
  preferred_viewer: dashboards-next
  crossfilter_enabled: true
  description: ''
  preferred_slug: gxHTNuEQsjW9IXCXEh3sYo
  elements:
  - name: ''
    type: text
    title_text: ''
    subtitle_text: ''
    body_text: '[{"type":"p","children":[{"text":"Revenue Contribution Analysis talks
      about how the change in a set of key drivers impacts the revenue. Below is the
      formula of Revenue for an ecommerce company","fontSize":"12pt","backgroundColor":"transparent","color":"rgb(67,
      67, 67)"}],"id":1692689808832},{"type":"h3","id":1692701389968,"children":[{"text":"\n"},{"text":"Revenue
      =  Users   x    Conversion Rate   x   Cart Size   x   Average Price","color":"hsl(210,
      70%, 61%)","bold":true,"underline":true}],"align":"center"},{"type":"p","children":[{"text":""}],"id":1692701369896},{"type":"ol","children":[{"type":"li","children":[{"type":"lic","children":[{"text":"Users
      = Number of users who visited our website and viewed a product.  "}],"id":1692689888446}],"id":1692689894895},{"type":"li","children":[{"type":"lic","id":1692689899270,"children":[{"text":"Conversion
      Rate =  %age of vistors who bought a product. "}]}],"id":1692689899271},{"type":"li","children":[{"type":"lic","children":[{"text":"Cart
      Size = Number of products in a cart for any transaction"}],"id":1692689888447}],"id":1692689894882},{"type":"li","children":[{"type":"lic","id":1692689914115,"children":[{"text":"Average
      Price  =  Average revenue per product"}]}],"id":1692689914117}],"id":1692689894898},{"type":"ul","children":[{"type":"li","children":[{"type":"lic","children":[{"text":"With
      the help of LMDI growth decomposition method, we estimate revenue change over
      any time period as: "}],"id":1692689888448}],"id":1692701415600}],"id":1692701415599},{"type":"p","children":[{"text":"Δ
      ","fontSize":"20px","backgroundColor":"rgba(80, 151, 255, 0.18)","color":"hsl(210,
      76%, 48%)","underline":true,"bold":true},{"text":"Revenue  =   Revenue Change
      Contribution  by   ","color":"hsl(210, 76%, 48%)","underline":true,"bold":true},{"text":"Δ","fontSize":"20px","backgroundColor":"rgba(80,
      151, 255, 0.18)","color":"hsl(210, 76%, 48%)","underline":true,"bold":true},{"text":"
      (Users ) + ","color":"hsl(210, 76%, 48%)","underline":true,"bold":true},{"text":"Δ","fontSize":"20px","backgroundColor":"rgba(80,
      151, 255, 0.18)","color":"hsl(210, 76%, 48%)","underline":true,"bold":true},{"text":"
      (Conv Rate) + ","color":"hsl(210, 76%, 48%)","underline":true,"bold":true},{"text":"Δ","fontSize":"20px","backgroundColor":"rgba(80,
      151, 255, 0.18)","color":"hsl(210, 76%, 48%)","underline":true,"bold":true},{"text":"(Cart
      Size) + ","color":"hsl(210, 76%, 48%)","underline":true,"bold":true},{"text":"Δ","fontSize":"20px","backgroundColor":"rgba(80,
      151, 255, 0.18)","color":"hsl(210, 76%, 48%)","underline":true,"bold":true},{"text":"
      (Average Price) ","color":"hsl(210, 76%, 48%)","underline":true,"bold":true}],"id":1692689888449,"align":"center"}]'
    rich_content_json: '{"format":"slate"}'
    row: 2
    col: 0
    width: 24
    height: 6
  - title: Conversion Rate
    name: Conversion Rate
    model: ecom_ga4
    explore: sessions
    type: single_value
    fields: [total_users, purchase_users]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: measure
      expression: ''
      label: Total Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: total_users
      type: count_distinct
      _type_hint: number
    - category: table_calculation
      expression: "${purchase_users}/${total_users}"
      label: Conversion Rate
      value_format:
      value_format_name: percent_2
      _kind_hint: measure
      table_calculation: conversion_rate
      _type_hint: number
    - category: measure
      expression: ''
      label: Purchase Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: purchase_users
      type: count_distinct
      _type_hint: number
      filters:
        sessions.event_name: purchase
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: ''
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: [count_of_user_pseudo_id, total_users, purchase_users]
    hidden_pivots: {}
    listen:
      City: sessions.city
      Item Name: sessions.item_name
      Source: sessions.source
      Medium: sessions.medium
      Region: sessions.region
      Country: sessions.country
    row: 9
    col: 20
    width: 4
    height: 2
  - title: Monthly Revenue Trend
    name: Monthly Revenue Trend
    model: ecom_ga4
    explore: sessions
    type: looker_line
    fields: [sessions.session_month, sum_of_purchase_revenue]
    fill_fields: [sessions.session_month]
    sorts: [sessions.session_month desc]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: measure
      expression: ''
      label: Sum of Purchase Revenue
      value_format:
      value_format_name: usd_0
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: sum_of_purchase_revenue
      type: sum
      _type_hint: number
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: circle
    show_value_labels: true
    label_density: 25
    x_axis_scale: time
    y_axis_combined: true
    show_null_points: true
    interpolation: linear
    color_application:
      collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
      palette_id: 5d189dfc-4f46-46f3-822b-bfb0b61777b1
      options:
        steps: 5
    y_axes: [{label: '', orientation: left, series: [{axisId: sum_of_purchase_revenue,
            id: sum_of_purchase_revenue, name: Revenue}], showLabels: true, showValues: true,
        unpinAxis: false, tickDensity: default, tickDensityCustom: 5, type: linear}]
    x_axis_zoom: true
    y_axis_zoom: true
    label_value_format: 0,,"M"
    series_colors: {}
    series_labels:
      sum_of_purchase_revenue: Revenue
    series_point_styles:
      sum_of_purchase_revenue: auto
    x_axis_datetime_label: ''
    reference_lines: []
    trend_lines: [{color: "#e8e54c", label_position: right, order: 3, period: 7, regression_type: linear,
        series_index: 1, show_label: false}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: ''
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: []
    hidden_pivots: {}
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    title_hidden: true
    listen:
      City: sessions.city
      Item Name: sessions.item_name
      Source: sessions.source
      Medium: sessions.medium
      Region: sessions.region
      Country: sessions.country
    row: 12
    col: 0
    width: 24
    height: 6
  - title: Revenue
    name: Revenue
    model: ecom_ga4
    explore: sessions
    type: single_value
    fields: [sum_of_purchase_revenue]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - measure: sum_of_purchase_revenue
      based_on: sessions.purchase_revenue
      expression: ''
      label: Sum of Purchase Revenue
      type: sum
      _kind_hint: measure
      _type_hint: number
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: '"$" #,##0,,"M"'
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields:
    hidden_pivots: {}
    listen:
      City: sessions.city
      Item Name: sessions.item_name
      Source: sessions.source
      Medium: sessions.medium
      Region: sessions.region
      Country: sessions.country
    row: 9
    col: 0
    width: 6
    height: 2
  - title: Users
    name: Users
    model: ecom_ga4
    explore: sessions
    type: single_value
    fields: [count_of_user_pseudo_id]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - measure: count_of_user_pseudo_id
      based_on: sessions.user_pseudo_id
      expression: ''
      label: Count of User Pseudo ID
      type: count_distinct
      _kind_hint: measure
      _type_hint: number
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: "#,##0"
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: []
    hidden_pivots: {}
    listen:
      City: sessions.city
      Item Name: sessions.item_name
      Source: sessions.source
      Medium: sessions.medium
      Region: sessions.region
      Country: sessions.country
    row: 9
    col: 6
    width: 4
    height: 2
  - title: Purchase Quantity
    name: Purchase Quantity
    model: ecom_ga4
    explore: sessions
    type: single_value
    fields: [product_quantity]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: measure
      expression: ''
      label: Product Quantity
      based_on: sessions.quantity
      _kind_hint: measure
      measure: product_quantity
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: ''
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: []
    hidden_pivots: {}
    listen:
      City: sessions.city
      Item Name: sessions.item_name
      Source: sessions.source
      Medium: sessions.medium
      Region: sessions.region
      Country: sessions.country
    row: 9
    col: 10
    width: 4
    height: 2
  - title: Average Price
    name: Average Price
    model: ecom_ga4
    explore: sessions
    type: single_value
    fields: [sum_of_purchase_revenue, product_quantity]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}/${product_quantity}"
      label: Average Price
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: average_price
      _type_hint: number
    - measure: sum_of_purchase_revenue
      based_on: sessions.purchase_revenue
      expression: ''
      label: Sum of Purchase Revenue
      type: sum
      _kind_hint: measure
      _type_hint: number
    - category: measure
      expression: ''
      label: Product Quantity
      based_on: sessions.quantity
      _kind_hint: measure
      measure: product_quantity
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: ''
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: [sum_of_purchase_revenue, sum_of_quantity, product_quantity]
    hidden_pivots: {}
    listen:
      City: sessions.city
      Item Name: sessions.item_name
      Source: sessions.source
      Medium: sessions.medium
      Region: sessions.region
      Country: sessions.country
    row: 9
    col: 14
    width: 6
    height: 2
  - title: Monthly Revenue Change
    name: Monthly Revenue Change
    model: ecom_ga4
    explore: sessions
    type: looker_column
    fields: [sessions.session_month, purchase_revenue_current_period]
    fill_fields: [sessions.session_month]
    sorts: [sessions.session_month desc]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: measure
      expression: ''
      label: Purchase Revenue - Current Period
      value_format: 0,,"M"
      value_format_name: __custom
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: purchase_revenue_current_period
      type: sum
      _type_hint: number
    - category: table_calculation
      label: "% change Past Period"
      value_format:
      value_format_name: percent_0
      calculation_type: percent_difference_from_previous
      table_calculation: change_past_period
      args:
      - purchase_revenue_current_period
      _kind_hint: measure
      _type_hint: number
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: circle_outline
    show_value_labels: true
    label_density: 25
    x_axis_scale: time
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    color_application:
      collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
      palette_id: e34425a5-3228-4f76-b45d-2e7cd13a6766
      options:
        steps: 5
        reverse: false
    y_axes: [{label: '', orientation: left, series: [{axisId: sum_of_purchase_revenue,
            id: sum_of_purchase_revenue, name: Revenue}], showLabels: true, showValues: true,
        unpinAxis: false, tickDensity: default, tickDensityCustom: 5, type: linear},
      {label: '', orientation: left, series: [{axisId: percent_change_from_previous_sum_of_purchase_revenue_copy,
            id: percent_change_from_previous_sum_of_purchase_revenue_copy, name: Percent
              change from previous -  Sum of Purchase Revenue Copy}], showLabels: true,
        showValues: true, unpinAxis: true, tickDensity: default, type: linear}]
    x_axis_zoom: true
    y_axis_zoom: true
    series_colors: {}
    label_color: []
    x_axis_datetime_label: ''
    reference_lines: []
    trend_lines: []
    up_color: "#7CB342"
    down_color: "#FF8168"
    total_color: "#F9AB00"
    show_null_points: true
    interpolation: linear
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: ''
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: [sum_of_purchase_revenue, purchase_revenue_current_period]
    hidden_pivots: {}
    title_hidden: true
    listen:
      City: sessions.city
      Item Name: sessions.item_name
      Source: sessions.source
      Medium: sessions.medium
      Region: sessions.region
      Country: sessions.country
    row: 19
    col: 0
    width: 24
    height: 6
  - title: Cart Size
    name: Cart Size
    model: ecom_ga4
    explore: sessions
    type: looker_line
    fields: [sessions.session_month, count_of_transaction_id, sum_of_purchase_revenue,
      product_quantity, purchase_revenue_current, total_users, purchase_users, purchase_quantity]
    fill_fields: [sessions.session_month]
    sorts: [sessions.session_month]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: measure
      expression: ''
      label: Count of Transaction ID
      based_on: sessions.transaction_id
      _kind_hint: measure
      measure: count_of_transaction_id
      type: count_distinct
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Sum of Purchase Revenue
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: sum_of_purchase_revenue
      type: sum
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Product Quantity
      based_on: sessions.quantity
      _kind_hint: measure
      measure: product_quantity
      type: sum
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Purchase Revenue - Current
      value_format:
      value_format_name: decimal_0
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: purchase_revenue_current
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Total Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: total_users
      type: count_distinct
      _type_hint: number
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}-offset(${sum_of_purchase_revenue},-1)"
      label: Revenue Change
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: revenue_change
      _type_hint: number
    - category: table_calculation
      expression: "${purchase_users}/${total_users}"
      label: Conversion Rate
      value_format:
      value_format_name: percent_0
      _kind_hint: measure
      table_calculation: conversion_rate
      _type_hint: number
    - category: table_calculation
      expression: "${purchase_quantity}/${count_of_transaction_id}"
      label: Cart Size
      value_format:
      value_format_name: decimal_2
      _kind_hint: measure
      table_calculation: cart_size
      _type_hint: number
    - category: measure
      expression: ''
      label: Purchase Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: purchase_users
      type: count_distinct
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}/${purchase_quantity}"
      label: Average Price
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: average_price
      _type_hint: number
    - category: table_calculation
      expression: "${revenue_change}/(log(${purchase_revenue_current})-log(offset(${purchase_revenue_current},-1)))"
      label: Multiplying Factor
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: multiplying_factor
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${average_price}/offset(${average_price},-1)))"
      label: Average Price Contribution
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: average_price_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${conversion_rate}/offset(${conversion_rate},-1)))"
      label: Conversion Rate Contribution
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: conversion_rate_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${cart_size}/offset(${cart_size},-1)))"
      label: 'Cart Size Contribution '
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: cart_size_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${total_users}/offset(${total_users},-1)))"
      label: 'Users Contribution  '
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: users_contribution
      _type_hint: number
    - category: measure
      expression:
      label: Purchase Quantity
      value_format:
      value_format_name:
      based_on: sessions.quantity
      _kind_hint: measure
      measure: purchase_quantity
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: circle_outline
    show_value_labels: true
    label_density: 25
    x_axis_scale: time
    y_axis_combined: true
    show_null_points: true
    interpolation: linear
    color_application:
      collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
      palette_id: 5d189dfc-4f46-46f3-822b-bfb0b61777b1
      options:
        steps: 5
    y_axes: [{label: '', orientation: left, series: [{axisId: average_price_contribution,
            id: average_price_contribution, name: Average Price Contribution}, {axisId: conversion_rate_contribution,
            id: conversion_rate_contribution, name: Conversion Rate Contribution},
          {axisId: cart_size_contribution, id: cart_size_contribution, name: 'Cart
              Size Contribution '}, {axisId: users_contribution, id: users_contribution,
            name: 'Users Contribution  '}], showLabels: true, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear}]
    x_axis_zoom: true
    y_axis_zoom: true
    font_size: '14'
    series_types:
      conversion_rate: area
      cart_size: area
    series_colors: {}
    series_labels:
      sum_of_purchase_revenue: Revenue
    series_point_styles:
      sum_of_purchase_revenue: auto
      conversion_rate: diamond
      cart_size: diamond
    x_axis_datetime_label: ''
    reference_lines: []
    trend_lines: []
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: ''
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: [multiplying_factor, average_price, revenue_change, purchase_revenue_previous,
      purchase_users, total_users, purchase_revenue_current, sum_of_quantity, sum_of_purchase_revenue,
      count_of_transaction_id, average_price_contribution, conversion_rate_contribution,
      cart_size_contribution, users_contribution, conversion_rate, purchase_quantity,
      product_quantity]
    hidden_pivots: {}
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    listen:
      City: sessions.city
      Item Name: sessions.item_name
      Source: sessions.source
      Medium: sessions.medium
      Region: sessions.region
      Country: sessions.country
    row: 26
    col: 12
    width: 12
    height: 5
  - title: Users
    name: Users (2)
    model: ecom_ga4
    explore: sessions
    type: looker_line
    fields: [sessions.session_month, count_of_transaction_id, sum_of_purchase_revenue,
      sum_of_quantity, purchase_revenue_current, total_users, purchase_users]
    fill_fields: [sessions.session_month]
    sorts: [sessions.session_month]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: measure
      expression: ''
      label: Count of Transaction ID
      based_on: sessions.transaction_id
      _kind_hint: measure
      measure: count_of_transaction_id
      type: count_distinct
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Sum of Purchase Revenue
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: sum_of_purchase_revenue
      type: sum
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Sum of Quantity
      based_on: sessions.quantity
      _kind_hint: measure
      measure: sum_of_quantity
      type: sum
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Purchase Revenue - Current
      value_format: 0,,,"M"
      value_format_name: __custom
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: purchase_revenue_current
      type: sum
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Total Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: total_users
      type: count_distinct
      _type_hint: number
    - category: table_calculation
      expression: offset(${purchase_revenue_current},-1)
      label: Purchase Revenue - Previous
      value_format: 0,,,"M"
      value_format_name: __custom
      _kind_hint: measure
      table_calculation: purchase_revenue_previous
      _type_hint: number
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}-offset(${sum_of_purchase_revenue},-1)"
      label: Revenue Change
      value_format: 0,,,"M"
      value_format_name: __custom
      _kind_hint: measure
      table_calculation: revenue_change
      _type_hint: number
    - category: table_calculation
      expression: "${purchase_users}/${total_users}"
      label: Conversion Rate
      value_format:
      value_format_name: percent_0
      _kind_hint: measure
      table_calculation: conversion_rate
      _type_hint: number
    - category: table_calculation
      expression: "${sum_of_quantity}/${count_of_transaction_id}"
      label: Cart Size
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: cart_size
      _type_hint: number
    - category: measure
      expression: ''
      label: Purchase Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: purchase_users
      type: count_distinct
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}/${sum_of_quantity}"
      label: Average Price
      value_format:
      value_format_name: usd_0
      _kind_hint: measure
      table_calculation: average_price
      _type_hint: number
    - category: table_calculation
      expression: "${revenue_change}/(log(${purchase_revenue_current})-log(${purchase_revenue_previous}))"
      label: Multiplying Factor
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: multiplying_factor
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${average_price}/offset(${average_price},-1)))"
      label: Average Price Contribution
      value_format: 0,,,"M"
      value_format_name: __custom
      _kind_hint: measure
      table_calculation: average_price_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${conversion_rate}/offset(${conversion_rate},-1)))"
      label: Conversion Rate Contribution
      value_format: 0,,,"M"
      value_format_name: __custom
      _kind_hint: measure
      table_calculation: conversion_rate_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${cart_size}/offset(${cart_size},-1)))"
      label: 'Cart Size Contribution '
      value_format: 0,,,"M"
      value_format_name: __custom
      _kind_hint: measure
      table_calculation: cart_size_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${total_users}/offset(${total_users},-1)))"
      label: 'Users Contribution  '
      value_format: 0,,,"M"
      value_format_name: __custom
      _kind_hint: measure
      table_calculation: users_contribution
      _type_hint: number
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: circle_outline
    show_value_labels: true
    label_density: 25
    x_axis_scale: time
    y_axis_combined: true
    show_null_points: true
    interpolation: linear
    color_application:
      collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
      palette_id: 5d189dfc-4f46-46f3-822b-bfb0b61777b1
      options:
        steps: 5
    y_axes: [{label: '', orientation: left, series: [{axisId: average_price_contribution,
            id: average_price_contribution, name: Average Price Contribution}, {axisId: conversion_rate_contribution,
            id: conversion_rate_contribution, name: Conversion Rate Contribution},
          {axisId: cart_size_contribution, id: cart_size_contribution, name: 'Cart
              Size Contribution '}, {axisId: users_contribution, id: users_contribution,
            name: 'Users Contribution  '}], showLabels: true, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear}]
    x_axis_zoom: true
    y_axis_zoom: true
    font_size: '14'
    series_types:
      conversion_rate: area
      cart_size: area
      average_price: area
      total_users: area
    series_colors: {}
    series_labels:
      sum_of_purchase_revenue: Revenue
    series_point_styles:
      sum_of_purchase_revenue: auto
      conversion_rate: diamond
      cart_size: diamond
      average_price: diamond
      total_users: diamond
    x_axis_datetime_label: ''
    reference_lines: []
    trend_lines: []
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: ''
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: [multiplying_factor, revenue_change, purchase_revenue_previous,
      purchase_users, purchase_revenue_current, sum_of_quantity, sum_of_purchase_revenue,
      count_of_transaction_id, average_price_contribution, conversion_rate_contribution,
      cart_size_contribution, users_contribution, conversion_rate, cart_size, average_price]
    hidden_pivots: {}
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    listen:
      City: sessions.city
      Item Name: sessions.item_name
      Source: sessions.source
      Medium: sessions.medium
      Region: sessions.region
      Country: sessions.country
    row: 31
    col: 12
    width: 12
    height: 5
  - name: " (2)"
    type: text
    title_text: ''
    subtitle_text: ''
    body_text: '[{"type":"p","children":[{"text":""}],"id":1692959396774},{"type":"h1","children":[{"text":"INTRODUCTION","color":"hsl(244,
      100%, 50%)","bold":true,"underline":true}]}]'
    rich_content_json: '{"format":"slate"}'
    row: 0
    col: 10
    width: 14
    height: 2
  - name: ' <img src="https://w7pngwingcom/pngs/201/722/png-transparent-computer-icons-speedometer-dashboard-speedometer-angle-motorcycle-odometer-thumbnailpng"
      width="30" height="30">  Top Line Metrics</span> '
    type: text
    title_text: ' <img src="https://w7.pngwing.com/pngs/201/722/png-transparent-computer-icons-speedometer-dashboard-speedometer-angle-motorcycle-odometer-thumbnail.png"
      width="30" height="30">  Top Line Metrics</span> '
    subtitle_text: ''
    body_text: ''
    row: 8
    col: 0
    width: 24
    height: 1
  - name: '<img src="https://cdn-icons-pngflaticoncom/512/245/245899png" width="30"
      height="30">  MONTHLY REVENUE TREND</span> '
    type: text
    title_text: '<img src="https://cdn-icons-png.flaticon.com/512/245/245899.png"
      width="30" height="30">  MONTHLY REVENUE TREND</span> '
    subtitle_text: ''
    body_text: ''
    row: 11
    col: 0
    width: 24
    height: 1
  - name: '<img src="https://cdn-icons-pngflaticoncom/512/1162/1162430png" width="30"
      height="30">  CONTRIBUTING FACTORS TREND</span> '
    type: text
    title_text: '<img src="https://cdn-icons-png.flaticon.com/512/1162/1162430.png"
      width="30" height="30">  CONTRIBUTING FACTORS TREND</span> '
    subtitle_text: ''
    body_text: ''
    row: 25
    col: 0
    width: 24
    height: 1
  - name: '<img src="https://cdnimgbincom/25/16/3/statistics-icon-growth-icon-economy-icon-CHdP47bKjpg"
      width="30" height="30">  MONTHLY REVENUE GROWTH</span> '
    type: text
    title_text: '<img src="https://cdn.imgbin.com/25/16/3/statistics-icon-growth-icon-economy-icon-CHdP47bK.jpg"
      width="30" height="30">  MONTHLY REVENUE GROWTH</span> '
    subtitle_text: ''
    body_text: ''
    row: 18
    col: 0
    width: 24
    height: 1
  - name: '<img src="https://icon-librarycom/images/factor-icon/factor-icon-13jpg"
      width="30" height="30">  FACTOR CONTRIBUTION ANALYSIS</span>  '
    type: text
    title_text: '<img src="https://icon-library.com/images/factor-icon/factor-icon-13.jpg"
      width="30" height="30">  FACTOR CONTRIBUTION ANALYSIS</span>  '
    subtitle_text: ''
    body_text: "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;How\
      \ to read visual - Each bar shows contribution of the 4 underlying factors in\
      \ revenue change when compared to previous month"
    row: 36
    col: 0
    width: 24
    height: 2
  - name: <img src="https://cdn-icons-pngflaticoncom/512/4784/4784180png" width="60"
      height="60">
    type: text
    title_text: <img src="https://cdn-icons-png.flaticon.com/512/4784/4784180.png"
      width="60" height="60">
    subtitle_text: ''
    body_text: ''
    row: 0
    col: 7
    width: 3
    height: 2
  - type: button
    name: button_943
    rich_content_json: '{"text":"User Decomposition","description":"","newTab":true,"alignment":"center","size":"medium","style":"FILLED","color":"#12B5CB","href":"https://tatvicpartner.cloud.looker.com/dashboards/68?City=&Month=&Item%20Category=&Region=&Session%20Date=2017%2F01%2F01%20to%202017%2F10%2F01"}'
    row: 48
    col: 18
    width: 5
    height: 1
  - type: button
    name: button_944
    rich_content_json: '{"text":"Cart Size Decomposition","description":"","newTab":true,"alignment":"center","size":"medium","style":"FILLED","color":"#12B5CB","href":"https://tatvicpartner.cloud.looker.com/dashboards/65?City=&Month=&Item%20Name=&Region=&Country=&Session%20Date=2017%2F01%2F01%20to%202017%2F10%2F01"}'
    row: 48
    col: 6
    width: 6
    height: 1
  - type: button
    name: button_945
    rich_content_json: '{"text":"Average Price Decomposition","description":"","newTab":true,"alignment":"center","size":"medium","style":"FILLED","color":"#12B5CB","href":"https://tatvicpartner.cloud.looker.com/dashboards/64?City=&Month=&Item%20Category=&Region=&Session%20Date=2017%2F01%2F01%20to%202017%2F10%2F01"}'
    row: 48
    col: 0
    width: 6
    height: 1
  - type: button
    name: button_946
    rich_content_json: '{"text":"Conversion Rate Decomposition","description":"","newTab":true,"alignment":"center","size":"medium","style":"FILLED","color":"#12B5CB","href":"https://tatvicpartner.cloud.looker.com/dashboards/67?City=&Month=&Item%20Category=&Region=&Session%20Date=2017%2F01%2F01%20to%202017%2F10%2F01"}'
    row: 48
    col: 12
    width: 5
    height: 1
  - name: " (3)"
    type: text
    title_text: ''
    subtitle_text: ''
    body_text: '[{"type":"ul","children":[{"type":"li","children":[{"type":"lic","children":[{"text":"As
      we have gone through the overall analysis on combined factor contribution of
      each factor their individual trends, Let''s drill down further on each factor
      and gain further insights."}],"align":"center"}],"id":1693214057187}],"id":1693214057183}]'
    rich_content_json: '{"format":"slate"}'
    row: 46
    col: 0
    width: 24
    height: 2
  - title: Average Price
    name: Average Price (2)
    model: ecom_ga4
    explore: sessions
    type: looker_area
    fields: [sessions.session_month, count_of_transaction_id, sum_of_purchase_revenue,
      sum_of_quantity, purchase_revenue_current, total_users, purchase_users, purchase_quantity]
    fill_fields: [sessions.session_month]
    sorts: [sessions.session_month]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: measure
      expression: ''
      label: Count of Transaction ID
      based_on: sessions.transaction_id
      _kind_hint: measure
      measure: count_of_transaction_id
      type: count_distinct
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Sum of Purchase Revenue
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: sum_of_purchase_revenue
      type: sum
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Sum of Quantity
      based_on: sessions.quantity
      _kind_hint: measure
      measure: sum_of_quantity
      type: sum
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Purchase Revenue - Current
      value_format: 0,,,"M"
      value_format_name: __custom
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: purchase_revenue_current
      type: sum
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Total Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: total_users
      type: count_distinct
      _type_hint: number
    - category: table_calculation
      expression: offset(${purchase_revenue_current},-1)
      label: Purchase Revenue - Previous
      value_format: 0,,,"M"
      value_format_name: __custom
      _kind_hint: measure
      table_calculation: purchase_revenue_previous
      _type_hint: number
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}-offset(${sum_of_purchase_revenue},-1)"
      label: Revenue Change
      value_format: 0,,,"M"
      value_format_name: __custom
      _kind_hint: measure
      table_calculation: revenue_change
      _type_hint: number
    - category: table_calculation
      expression: "${purchase_users}/${total_users}"
      label: Conversion Rate
      value_format:
      value_format_name: percent_0
      _kind_hint: measure
      table_calculation: conversion_rate
      _type_hint: number
    - category: table_calculation
      expression: "${purchase_quantity}/${count_of_transaction_id}"
      label: Cart Size
      value_format:
      value_format_name: decimal_2
      _kind_hint: measure
      table_calculation: cart_size
      _type_hint: number
    - category: measure
      expression: ''
      label: Purchase Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: purchase_users
      type: count_distinct
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}/${purchase_quantity}"
      label: Average Price
      value_format:
      value_format_name: decimal_1
      _kind_hint: measure
      table_calculation: average_price
      _type_hint: number
    - category: table_calculation
      expression: "${revenue_change}/(log(${purchase_revenue_current})-log(${purchase_revenue_previous}))"
      label: Multiplying Factor
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: multiplying_factor
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${average_price}/offset(${average_price},-1)))"
      label: Average Price Contribution
      value_format: 0,,,"M"
      value_format_name: __custom
      _kind_hint: measure
      table_calculation: average_price_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${conversion_rate}/offset(${conversion_rate},-1)))"
      label: Conversion Rate Contribution
      value_format: 0,,,"M"
      value_format_name: __custom
      _kind_hint: measure
      table_calculation: conversion_rate_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${cart_size}/offset(${cart_size},-1)))"
      label: 'Cart Size Contribution '
      value_format: 0,,,"M"
      value_format_name: __custom
      _kind_hint: measure
      table_calculation: cart_size_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${total_users}/offset(${total_users},-1)))"
      label: 'Users Contribution  '
      value_format: 0,,,"M"
      value_format_name: __custom
      _kind_hint: measure
      table_calculation: users_contribution
      _type_hint: number
    - category: measure
      expression:
      label: Purchase Quantity
      value_format:
      value_format_name:
      based_on: sessions.quantity
      _kind_hint: measure
      measure: purchase_quantity
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: circle_outline
    show_value_labels: true
    label_density: 25
    x_axis_scale: time
    y_axis_combined: true
    show_null_points: true
    interpolation: linear
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    color_application:
      collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
      palette_id: 5d189dfc-4f46-46f3-822b-bfb0b61777b1
      options:
        steps: 5
    y_axes: [{label: '', orientation: left, series: [{axisId: average_price_contribution,
            id: average_price_contribution, name: Average Price Contribution}, {axisId: conversion_rate_contribution,
            id: conversion_rate_contribution, name: Conversion Rate Contribution},
          {axisId: cart_size_contribution, id: cart_size_contribution, name: 'Cart
              Size Contribution '}, {axisId: users_contribution, id: users_contribution,
            name: 'Users Contribution  '}], showLabels: true, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear}]
    x_axis_zoom: true
    y_axis_zoom: true
    font_size: '14'
    series_colors: {}
    series_labels:
      sum_of_purchase_revenue: Revenue
    series_point_styles:
      sum_of_purchase_revenue: auto
      conversion_rate: diamond
      cart_size: diamond
    x_axis_datetime_label: ''
    reference_lines: []
    trend_lines: []
    ordering: none
    show_null_labels: false
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: ''
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: [multiplying_factor, revenue_change, purchase_revenue_previous,
      purchase_users, total_users, purchase_revenue_current, sum_of_quantity, sum_of_purchase_revenue,
      count_of_transaction_id, average_price_contribution, conversion_rate_contribution,
      cart_size_contribution, users_contribution, conversion_rate, purchase_quantity,
      cart_size]
    hidden_pivots: {}
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    listen:
      City: sessions.city
      Item Name: sessions.item_name
      Source: sessions.source
      Medium: sessions.medium
      Region: sessions.region
      Country: sessions.country
    row: 26
    col: 0
    width: 12
    height: 5
  - title: Conversion Rate
    name: Conversion Rate (2)
    model: ecom_ga4
    explore: sessions
    type: looker_line
    fields: [sessions.session_month, count_of_transaction_id, sum_of_purchase_revenue,
      product_quantity, purchase_revenue_current, total_users, purchase_users, purchase_quantity]
    fill_fields: [sessions.session_month]
    sorts: [sessions.session_month]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: measure
      expression: ''
      label: Count of Transaction ID
      based_on: sessions.transaction_id
      _kind_hint: measure
      measure: count_of_transaction_id
      type: count_distinct
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Sum of Purchase Revenue
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: sum_of_purchase_revenue
      type: sum
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Product Quantity
      based_on: sessions.quantity
      _kind_hint: measure
      measure: product_quantity
      type: sum
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Purchase Revenue - Current
      value_format:
      value_format_name: decimal_0
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: purchase_revenue_current
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Total Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: total_users
      type: count_distinct
      _type_hint: number
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}-offset(${sum_of_purchase_revenue},-1)"
      label: Revenue Change
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: revenue_change
      _type_hint: number
    - category: table_calculation
      expression: "${purchase_users}/${total_users}"
      label: Conversion Rate
      value_format:
      value_format_name: percent_2
      _kind_hint: measure
      table_calculation: conversion_rate
      _type_hint: number
    - category: table_calculation
      expression: "${purchase_quantity}/${count_of_transaction_id}"
      label: Cart Size
      value_format:
      value_format_name: decimal_2
      _kind_hint: measure
      table_calculation: cart_size
      _type_hint: number
    - category: measure
      expression: ''
      label: Purchase Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: purchase_users
      type: count_distinct
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}/${purchase_quantity}"
      label: Average Price
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: average_price
      _type_hint: number
    - category: table_calculation
      expression: "${revenue_change}/(log(${purchase_revenue_current})-log(offset(${purchase_revenue_current},-1)))"
      label: Multiplying Factor
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: multiplying_factor
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${average_price}/offset(${average_price},-1)))"
      label: Average Price Contribution
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: average_price_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${conversion_rate}/offset(${conversion_rate},-1)))"
      label: Conversion Rate Contribution
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: conversion_rate_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${cart_size}/offset(${cart_size},-1)))"
      label: 'Cart Size Contribution '
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: cart_size_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${total_users}/offset(${total_users},-1)))"
      label: 'Users Contribution  '
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: users_contribution
      _type_hint: number
    - category: measure
      expression:
      label: Purchase Quantity
      value_format:
      value_format_name:
      based_on: sessions.quantity
      _kind_hint: measure
      measure: purchase_quantity
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: circle_outline
    show_value_labels: true
    label_density: 25
    x_axis_scale: time
    y_axis_combined: true
    show_null_points: true
    interpolation: linear
    color_application:
      collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
      palette_id: 5d189dfc-4f46-46f3-822b-bfb0b61777b1
      options:
        steps: 5
    y_axes: [{label: '', orientation: left, series: [{axisId: average_price_contribution,
            id: average_price_contribution, name: Average Price Contribution}, {axisId: conversion_rate_contribution,
            id: conversion_rate_contribution, name: Conversion Rate Contribution},
          {axisId: cart_size_contribution, id: cart_size_contribution, name: 'Cart
              Size Contribution '}, {axisId: users_contribution, id: users_contribution,
            name: 'Users Contribution  '}], showLabels: true, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear}]
    x_axis_zoom: true
    y_axis_zoom: true
    font_size: '14'
    series_types:
      conversion_rate: area
      cart_size: area
    series_colors: {}
    series_labels:
      sum_of_purchase_revenue: Revenue
    series_point_styles:
      sum_of_purchase_revenue: auto
      conversion_rate: diamond
      cart_size: diamond
    x_axis_datetime_label: ''
    reference_lines: []
    trend_lines: []
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: ''
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: [multiplying_factor, average_price, revenue_change, purchase_revenue_previous,
      purchase_users, total_users, purchase_revenue_current, sum_of_quantity, sum_of_purchase_revenue,
      count_of_transaction_id, average_price_contribution, conversion_rate_contribution,
      cart_size_contribution, users_contribution, purchase_quantity, product_quantity,
      cart_size]
    hidden_pivots: {}
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    listen:
      City: sessions.city
      Item Name: sessions.item_name
      Source: sessions.source
      Medium: sessions.medium
      Region: sessions.region
      Country: sessions.country
    row: 31
    col: 0
    width: 12
    height: 5
  - title: Factor Contribution - Revenue Change
    name: Factor Contribution - Revenue Change
    model: ecom_ga4
    explore: sessions
    type: looker_column
    fields: [sessions.session_month, count_of_transaction_id, sum_of_purchase_revenue,
      product_quantity, purchase_revenue_current, total_users, purchase_users, purchase_quantity]
    fill_fields: [sessions.session_month]
    sorts: [sessions.session_month]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: measure
      expression: ''
      label: Count of Transaction ID
      based_on: sessions.transaction_id
      _kind_hint: measure
      measure: count_of_transaction_id
      type: count_distinct
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Sum of Purchase Revenue
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: sum_of_purchase_revenue
      type: sum
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Product Quantity
      based_on: sessions.quantity
      _kind_hint: measure
      measure: product_quantity
      type: sum
      _type_hint: number
      filters:
        sessions.transaction_id: "-NULL"
    - category: measure
      expression: ''
      label: Purchase Revenue - Current
      value_format:
      value_format_name: decimal_0
      based_on: sessions.purchase_revenue
      _kind_hint: measure
      measure: purchase_revenue_current
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: measure
      expression: ''
      label: Total Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: total_users
      type: count_distinct
      _type_hint: number
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}-offset(${sum_of_purchase_revenue},-1)"
      label: Revenue Change
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: revenue_change
      _type_hint: number
    - category: table_calculation
      expression: "${purchase_users}/${total_users}"
      label: Conversion Rate
      value_format:
      value_format_name: percent_0
      _kind_hint: measure
      table_calculation: conversion_rate
      _type_hint: number
    - category: table_calculation
      expression: "${purchase_quantity}/${count_of_transaction_id}"
      label: Cart Size
      value_format:
      value_format_name: decimal_2
      _kind_hint: measure
      table_calculation: cart_size
      _type_hint: number
    - category: measure
      expression: ''
      label: Purchase Users
      based_on: sessions.user_pseudo_id
      _kind_hint: measure
      measure: purchase_users
      type: count_distinct
      _type_hint: number
      filters:
        sessions.event_name: purchase
    - category: table_calculation
      expression: "${sum_of_purchase_revenue}/${purchase_quantity}"
      label: Average Price
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: average_price
      _type_hint: number
    - category: table_calculation
      expression: "${revenue_change}/(log(${purchase_revenue_current})-log(offset(${purchase_revenue_current},-1)))"
      label: Multiplying Factor
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: multiplying_factor
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${average_price}/offset(${average_price},-1)))"
      label: Average Price Contribution
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: average_price_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${conversion_rate}/offset(${conversion_rate},-1)))"
      label: Conversion Rate Contribution
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: conversion_rate_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${cart_size}/offset(${cart_size},-1)))"
      label: 'Cart Size Contribution '
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: cart_size_contribution
      _type_hint: number
    - category: table_calculation
      expression: "${multiplying_factor}*(log(${total_users}/offset(${total_users},-1)))"
      label: 'Users Contribution  '
      value_format:
      value_format_name: decimal_0
      _kind_hint: measure
      table_calculation: users_contribution
      _type_hint: number
    - category: measure
      expression:
      label: Purchase Quantity
      value_format:
      value_format_name:
      based_on: sessions.quantity
      _kind_hint: measure
      measure: purchase_quantity
      type: sum
      _type_hint: number
      filters:
        sessions.event_name: purchase
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: percent
    limit_displayed_rows: false
    legend_position: center
    point_style: circle_outline
    show_value_labels: true
    label_density: 25
    x_axis_scale: time
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    color_application:
      collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
      palette_id: 5d189dfc-4f46-46f3-822b-bfb0b61777b1
      options:
        steps: 5
    y_axes: [{label: '', orientation: left, series: [{axisId: average_price_contribution,
            id: average_price_contribution, name: Average Price Contribution}, {axisId: conversion_rate_contribution,
            id: conversion_rate_contribution, name: Conversion Rate Contribution},
          {axisId: cart_size_contribution, id: cart_size_contribution, name: 'Cart
              Size Contribution '}, {axisId: users_contribution, id: users_contribution,
            name: 'Users Contribution  '}], showLabels: true, showValues: true, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear}]
    x_axis_zoom: true
    y_axis_zoom: true
    font_size: '14'
    series_colors:
      average_price_contribution: "#7CB342"
      cart_size_contribution: "#80868B"
      users_contribution: "#EA4335"
    series_labels:
      sum_of_purchase_revenue: Revenue
    series_point_styles:
      sum_of_purchase_revenue: auto
      conversion_rate: diamond
      cart_size: diamond
    x_axis_datetime_label: ''
    reference_lines: []
    trend_lines: []
    show_null_points: true
    interpolation: linear
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: ''
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: [multiplying_factor, average_price, revenue_change, purchase_revenue_previous,
      purchase_users, total_users, purchase_revenue_current, sum_of_quantity, sum_of_purchase_revenue,
      count_of_transaction_id, conversion_rate, purchase_quantity, product_quantity,
      cart_size]
    hidden_pivots: {}
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    listen:
      City: sessions.city
      Item Name: sessions.item_name
      Source: sessions.source
      Medium: sessions.medium
      Region: sessions.region
      Country: sessions.country
    row: 38
    col: 0
    width: 24
    height: 8
  filters:
  - name: City
    title: City
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: tag_list
      display: popover
    model: ecom_ga4
    explore: sessions
    listens_to_filters: []
    field: sessions.city
  - name: Item Name
    title: Item Name
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: tag_list
      display: popover
    model: ecom_ga4
    explore: sessions
    listens_to_filters: []
    field: sessions.item_name
  - name: Source
    title: Source
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: tag_list
      display: popover
    model: ecom_ga4
    explore: sessions
    listens_to_filters: []
    field: sessions.source
  - name: Medium
    title: Medium
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: tag_list
      display: popover
    model: ecom_ga4
    explore: sessions
    listens_to_filters: []
    field: sessions.medium
  - name: Region
    title: Region
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: tag_list
      display: popover
    model: ecom_ga4
    explore: sessions
    listens_to_filters: []
    field: sessions.region
  - name: Country
    title: Country
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: tag_list
      display: popover
    model: ecom_ga4
    explore: sessions
    listens_to_filters: []
    field: sessions.country
