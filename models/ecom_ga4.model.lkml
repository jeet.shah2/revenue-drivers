label: "Ecom Google Analytics 4"

include: "/dashboards/*.dashboard.lookml"
include: "/explores/**/*.explore.lkml"


connection: "@{CONNECTION}"

datagroup: ga4_ecom_datagroup {
  sql_trigger: SELECT FLOOR(((TIMESTAMP_DIFF(CURRENT_TIMESTAMP(),'1970-01-01 00:00:00',SECOND)) - 60*60*6)/(60*60*24));;
  max_cache_age: "1 hour"
}

persist_with: ga4_ecom_datagroup
