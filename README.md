
# What does this Looker Block do for me?
This looker block is about analysis of how changes in a set of key drivers impacts the ecommerce revenue. Under  this framework, we express Revenue as -

#### Revenue = Users x Conversion Rate x Cart Size x Average Price

Where,
  1. Users = Number of users who visited our website.
  2. Conversion Rate =  % of visitors who bought a product.
  3. Cart Size = Number of products in a cart for any transaction
  4. Average Price  =  Average revenue per product

We can further break down the equation by expanding each of its component as -

####  Revenue = Users x (Transactions / Users) x (Items Sold / Transactions) x (Revenue / Items Sold)

Hence, technically, this means one of the 4 mentioned drivers in a given period (when compared to the previous period) can really drive the growth of revenue. An e-commerce store can have unique strategies that they can deploy to drive one of the key drivers.

With the help of LMDI growth decomposition method, we estimate revenue change over any time period as:

#### Δ Revenue  =   Revenue Change Contri by Δ (Users ) + Δ (Conv Rate) + Δ(Cart Size) + Δ (Average Price)

# Block Structure
This block is further divided into a set of dashboards. The first dashboard represents the summary of how change in revenue is driven by the above mentioned 4 components. The visualizations will also help understand the month on month trend in revenue as well as all the 4 key drivers impacting it. We can use filters to analyze how that revenue is divided across various marketing channels, device categories, regions and product categories.

The next 4 dashboards help users to deep dive further into the 4 key drives bringing the change to revenue. Apart from the trend on a specific key driver’s contribution to revenue, we can decompose it down further at levels like device categories, marketing channels and product categories along with geographic attributes like region.

Once we have deep dived into the last level , in order to drive actions to achieve growth in revenue by a specific key driver there are certain steps which marketers or product managers can evaluate, for e.g.

### Cart Size led revenue growth:
  1. Provide minimum purchase discount
  2. Limited time offer basis invoice size
  3. Product Bundling

### Price-led revenue growth:
  1. Premium product offerings
  2. Recommend add-ons, upgrades at cart
  3. Promote limited edition Products

### User-base led revenue growth:
  1. First-time visit / new user offers
  2. Remarketing /retargeting any drop-offs
  3. SEO opportunity

### Conv Rate led revenue growth:
  1. Drill down in products - Highlight revenue drivers on the website.
  2. Drill-down in campaigns for high performing campaigns


# Instructions to connect the block -

1. Select the Bigquery connection where you have the GA4 database.
2. Add the default database (data schema) from Bigquery in Looker.
Once done, the dashboards should be available to you.

  Prerequisite - You should have GA4 ecommerce implementation and key events as below -


# Looker Model & Views

### File Structure :

The elements comprising the Revenue Contribution Analysis Block are organized into directories based on file type or purpose.

1. Dashboards: All included LookML dashboards are present in this folder.
2. Explores: Some parts of the model file have been set aside for future use or improvement. The default part is called "session."
3. Models: The {ecom_ga4}.model file is present here. It includes the sessions explore, the LookML dashboards, and a data group specific to this model.
4. Views : {Session.view} - This is the base view of the sessions explore of the {ecom_ga4} model. It undertakes the extraction of nested GA4 data, subsequently flattening and incrementally storing it into an SQL-derived table.
5. manifest.lkml is present in the root directory, and is where the instance constants are defined.

# How Tables Are Structured?
This SQL query creates an incremental persistent derived table that supports the addition of new records without a complete drop and recreation of the table. Incremental updates are based on a specific field called "Session Date." To accommodate any potential delays in receiving the complete dataset, an increment offset of 3 is established. This implies that each scheduled run can introduce new data for a maximum of 3 days

Initial table construction SQL query that essentially inserts "1=1" across all event tables within the target dataset. In subsequent runs, the query is adapted to include appropriate "WHERE" conditions, thereby constraining the results to the specific date range dictated by the "increment_offset" value
